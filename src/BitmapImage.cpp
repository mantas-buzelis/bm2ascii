#include "BitmapImage.h"
#include<fstream>


#include <algorithm>

template <class T>
void endswap(T *objp)
{
  unsigned char *memp = reinterpret_cast<unsigned char*>(objp);
  std::reverse(memp, memp + sizeof(T));
}

bool BitmapImage::LoadFromFile(const char* file_name)
{
  std::ifstream in_file;
  in_file.open(file_name, std::ios::binary);

  if(in_file.fail())
  {
    return false;
  }

   in_file.read(m_header.file_prefix.data(), 2);//m_header.file_prefix.size());
   endswap(m_header.file_prefix.data());
   in_file.read(m_header.file_size.data(), m_header.file_size.size());
   //in_file.read(m_header.high_reserved.data(), m_header.high_reserved.size());
  return true;
}



const char* BitmapImage::GetFilePrefix() const
{
  return m_header.file_prefix.data();
}

BitmapImage::BitmapImage(const char* file_name)
{
  LoadFromFile(file_name);
}
