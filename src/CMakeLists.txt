cmake_minimum_required(VERSION 3.10)

project(wm2ascii CXX)
set(CMAKE_INSTALL_PREFIX ${PROJECT_SOURCE_DIR})
set(DIVISIBLE_INSTALL_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)
set(DIVISIBLE_INSTALL_BIN_DIR ${PROJECT_SOURCE_DIR}/bin)
set(
        CMAKE_RUNTIME_OUTPUT_DIRECTORY
            ${CMAKE_HOME_DIRECTORY}/bin
                )
include_directories(${DIVISIBLE_INSTALL_INCLUDE_DIR})
include_directories(${DIVISION_HEADERS_DIR})

include_directories(
            ${PROJECT_SOURCE_DIR}/include
            ${PROJECT_SOURCE_DIR}/src)

file(GLOB all_SRCS
    "${PROJECT_SOURCE_DIR}/include/*.h"
    "${PROJECT_SOURCE_DIR}/include/*.hpp"
    "${PROJECT_SOURCE_DIR}/src/*.cpp"
    "${PROJECT_SOURCE_DIR}/src/*.c")

add_executable(wm2ascii ${all_SRCS})


