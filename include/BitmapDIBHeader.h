#pragma once

#include <array>
#include <cstdint>

struct BitmapDIBHeader
{
  std::array<char,4> header_size;
  std::array<char,4> bitmap_width_pixels;
  std::array<char,4> bitmap_height_pixels;
  std::array<char,2> color_plane_count;
  std::array<char,2> bits_per_pixel;
  std::array<char,4> compression_method;
  std::array<char,4> image_raw_data_size;
  int32_t              horizontal_resolution;
  int32_t              vertical_resolution;
  std::array<char,4> colors_in_palette;
  std::array<char,4> important_colour_count;
};
