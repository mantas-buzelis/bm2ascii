#pragma once

struct Pixel
{
  char R;
  char G;
  char B;
  char A;
};
