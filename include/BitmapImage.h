#include <cstddef>
#include <vector>
#include "BitmapHeader.h"
#include "BitmapDIBHeader.h"
#include "Pixel.h"
class BitmapImage
{
  BitmapHeader m_header;
  BitmapDIBHeader m_DIBHeader;
  std::vector<Pixel> m_image_data;

  public:

  BitmapImage(BitmapHeader header, BitmapDIBHeader DIBHeader, const std::vector<Pixel>& image_data);

  BitmapImage(const char* file_name);

  BitmapImage();

  bool LoadFromFile(const char* file_name);

  void WriteToFile(const char* file_name);

  const char* GetFilePrefix() const;
};

