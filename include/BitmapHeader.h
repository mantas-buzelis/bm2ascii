#pragma once
#include <array>

struct BitmapHeader
{
  std::array<char,2> file_prefix;
  std::array<char,4> file_size;
  std::array<char,2> low_reserved;
  std::array<char,2> high_reserved;
  std::array<char,4> image_data_offset;
};
